---
title: 짝수는 싫어요
date : 2023-01-05T12:00:06+09:00
description: 짝수는 싫어요
draft: false
hideToc: false
enableToc: true
enableTocContent: true
categories : 
- 'Programmers'
- 'Programmers Level 0'
libraries:
- katex
---

[바로가기](https://school.programmers.co.kr/learn/courses/30/lessons/120813)

언어별 배열을 다루는 방법에 대한 문제입니다.

조건에 맞는 경우에 원소를 추가하는 방법을 사용하면 됩니다.

이 경우엔 1부터 시작하여 2씩 건너뛰며 n 이하의 수를 배열에 추가하면 됩니다.

``` C++
#include <string>
#include <vector>

using namespace std;

vector<int> solution(int n) {
    vector<int> answer;

    for (int i = 1; i<=n; i+=2)
    {
        answer.emplace_back(i);
    }
    return answer;
}
```

``` Python 3
def solution(n):
    answer = [v for v in range(1, n + 1, 2)]
    return answer
```

``` Java
class Solution {
    public int[] solution(int n) {
        int[] answer = new int[(n + 1) / 2];
        for(int i = 1; i <= n; i+=2)
        {
            answer[(i - 1) / 2] = i;
        }
        return answer;
    }
}
```

``` JavaScript
function solution(n) {
    var answer = [];

    for (let i = 1; i<=n; i+=2)
    {
        answer.push(i)
    }
    return answer;
}
```