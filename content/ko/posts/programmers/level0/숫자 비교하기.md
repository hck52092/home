---
title: 숫자 비교하기
date : 2022-12-26T12:00:06+09:00
description: 숫자 비교하기
draft: false
hideToc: false
enableToc: true
enableTocContent: true
categories : 
- 'Programmers'
- 'Programmers Level 0'
libraries:
- katex
---

[바로가기](https://school.programmers.co.kr/learn/courses/30/lessons/120807)

언어별 조건문을 어떻게 사용하는지에 대한 문제입니다.

if문으로 처리를 해도 되지만 여기서는 각 언어별 3항 연산자를 사용하도록 하겠습니다.


``` C++
#include <string>
#include <vector>

using namespace std;

int solution(int num1, int num2) {
    int answer = (num1 == num2) ? 1 : -1;
    return answer;
}
```

``` Python 3
def solution(num1, num2):
    answer = 1 if num1 == num2 else -1
    return answer
```

``` Java
class Solution {
    public int solution(int num1, int num2) {
        int answer = (num1 == num2) ? 1 : -1;
        return answer;
    }
}
```

``` JavaScript
function solution(num1, num2) {
    var answer = (num1 == num2) ? 1 : -1;
    return answer;
}
```