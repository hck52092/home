---
title: 몫 구하기
date : 2022-12-19T12:00:06+09:00
description: 몫 구하기
draft: false
hideToc: false
enableToc: true
enableTocContent: true
categories : 
- 'Programmers'
- 'Programmers Level 0'
libraries:
- katex
---

[바로가기](https://school.programmers.co.kr/learn/courses/30/lessons/120805)

언어별 기본적인 연산을 어떻게 하는지에 대한 문제입니다.

C++ / Java는 변수의 자료형이 실수냐 정수냐에 따라 나누기 연산 (/)의 방법이 다릅니다.
이 문제에서는 변수의 타입이 모두 정수이므로 정수의 나눗셈에 의하여 몫을 구할 수 있습니다.

Python의 경우에는 나머지 연산자가 두 종류로 나뉩니다.
일반적인 /의 경우 숫자를 실수 취급하여 계산합니다. 즉 7/2는 3.5가 됩니다.
몫을 구하기 위해서는 // 를 써야합니다. (다른 언어들에서는 //가 대체로 주석 기호로 쓰입니다.) 

JavaScript는 몫을 구하는 연산자가 없습니다. 따라서 소수점 이하를 버리는 방법을 사용해야합니다.
세가지 방법정도를 생각해볼 수 있습니다. 
- 내림을 해주는 Math.floor 함수
- 정수로 형변환을 해주는(정수로 바꾸면서 소수부를 버려줍니다) parseInt 함수
- 소수부를 만드는 나머지 부분을 미리 빼준 후 나누어서 나누어 떨어지게 만드는 방법

예시 답안에는 3번째를 사용하도록 하겠습니다.


``` C++
#include <string>
#include <vector>

using namespace std;

int solution(int num1, int num2) {
    int answer = num1 / num2;
    return answer;
}
```

``` Python 3
def solution(num1, num2):
    answer = num1 // num2
    return answer
```

``` Java
class Solution {
    public int solution(int num1, int num2) {
        int answer = num1 / num2;
        return answer;
    }
}
```

``` JavaScript
function solution(num1, num2) {
    var answer = (num1 - (num1 % num2)) / num2;
    return answer;
}
```