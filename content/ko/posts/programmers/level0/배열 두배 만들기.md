---
title: 배열 두배 만들기
date : 2022-12-26T12:00:06+09:00
description: 배열 두배 만들기
draft: false
hideToc: false
enableToc: true
enableTocContent: true
categories : 
- 'Programmers'
- 'Programmers Level 0'
libraries:
- katex
---

[바로가기](https://school.programmers.co.kr/learn/courses/30/lessons/120807)

언어별 배열 사용 대한 문제입니다.

C++ / Java는 원래 배열을 두 배로 바꿔주는 방법으로,

JavaScript / Python 3는 두 배로 키운 값을 갖는 새 배열을 만드는 방법으로 풀어보았습니다.

Python 3의 경우 List Comprehension 이라는 문법을 이용하면 좀 더 빠른 코드를 만들 수 있습니다.


``` C++
#include <string>
#include <vector>

using namespace std;

vector<int> solution(vector<int> numbers) {
    for(auto& v : numbers)
    {
        v *=2;
    }
    
    return numbers;
}
```

``` Python 3
def solution(numbers):
    answer = [v*2 for v in numbers]
    return answer
```

``` Java
class Solution {
    public int[] solution(int[] numbers) {
        for(int i = 0 ; i < numbers.length; ++i)
        {
            numbers[i] *= 2;
        }
        return numbers;
    }
}
```

``` JavaScript
function solution(numbers) {
    answer = numbers.map(function(v){return v*2});
    return answer;
}
```