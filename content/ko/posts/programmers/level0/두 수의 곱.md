---
title: 두 수의 곱
date : 2022-12-19T12:00:06+09:00
description: 두 수의 곱
draft: false
hideToc: false
enableToc: true
enableTocContent: true
categories : 
- 'Programmers'
- 'Programmers Level 0'
libraries:
- katex
---

[바로가기](https://school.programmers.co.kr/learn/courses/30/lessons/120804)

언어별 기본적인 연산을 어떻게 하는지에 대한 문제입니다.


``` C++
#include <string>
#include <vector>

using namespace std;

int solution(int num1, int num2) {
    int answer = num1 * num2;
    return answer;
}
```

``` Python 3
def solution(num1, num2):
    answer = num1 * num2
    return answer
```

``` Java
class Solution {
    public int solution(int num1, int num2) {
        int answer = num1 * num2;
        return answer;
    }
}
```

``` JavaScript
function solution(num1, num2) {
    var answer = num1 * num2;
    return answer;
}
```