+++
title = "About"
description = "About this blog"
type = "about"
date = "2022-07-24"
+++

그때 그때 생기는 관심사들을 정리할 겸 쌓아두고 나중에 필요하거나 생각날 때 다시 꺼내보는 창고 용도의 블로그입니다

[Hugo](https://gohugo.io/)와 [Zzo theme](https://github.com/zzossig/hugo-theme-zzo)를 이용하여 만들었습니다